package com.codecool.wereneke;

import java.sql.Timestamp;

public class Ping {

    private Timestamp timestamp;

    public Ping(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
