package com.codecool.wereneke;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@RestController
public class PingController {

    @RequestMapping(method = RequestMethod.GET, path = "/ping")
    public Ping ping() {
        Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
        return new Ping(timestamp);
    }
}
